FROM alpine

RUN apk add --no-cache mysql-client

COPY ./backup /etc/periodic/daily/

VOLUME /data

CMD crond -fL8
